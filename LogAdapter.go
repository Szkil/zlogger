package ZLogger

/*
- All keys should be defined in lowercase
- todo: I would like all keys to be wrapped in a []

*/

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"time"
)


// Adapters to hel record the data into place.
type LogAdapter interface {
	AsPK() int64
	AsType() string // what type it is or a classname this is going to be important over time and really should be a base line func
	AsRC() string   // returns the recode that most BO's has
	// AsFieldDump []Fields // something that can auto dump with out reflection
}

func Z_LogAdapter(i interface{}) (LogAdapter, bool) {
	if iInt, ok := i.(LogAdapter); ok {
		return iInt, true
	} else {
		return nil, false
	}
}


type TestcaseLog struct {
	Name string `json:"name"`
	Starttime string `json:"starttime"` // lets see if I need it for math
	Endtime string `json:"endtime"`
	Timetaken int64 `json:"timetaken"` // maybe int in seconds
	Result string `json:"result"` // Pass fail incomplete ???
	Errormsg string `json:"errormsg"`
	Description string `json:"description"`
	Datadump string `json:"datadump"`
	Load int `json:"load"`

	start time.Time
	end time.Time
}

func TCLogStart(name string) *TestcaseLog {
	liResult := new(TestcaseLog)
	liResult.Name = name

	liResult.start = time.Now()
	liResult.Starttime = liResult.start.Format(time.RFC3339)

	GetLogger().LOG().WithFields(log.Fields{
		"name": liResult.Name,
		"starttime": liResult.Starttime,
	}).Info("tcLogstart")

	return liResult
}

func (self *TestcaseLog) TCLogEnd(result string, datadump string) {
	self.end = time.Now()
	self.Timetaken = int64(time.Since( self.start ))
	self.Endtime = self.end.Format(time.RFC3339)
   self.Datadump = datadump
   self.Result = result
	// ZLogger.Debug1("Testcase dump Completed ")
	// Dump everything into the Logger
	GetLogger().LOG().WithFields(log.Fields{
		"name": self.Name,
		"starttime": self.Starttime,
		"endtime" : self.Endtime,
		"timetaken" : self.Timetaken,
		"result" : self.Result,
		"errormsg" : self.Errormsg,
		"description" : self.Description,
		"datadump" : self.Datadump,
		"load" : self.Load,
	}).Info("tclogend")

}

// Interface to read a struct of values but the adapter can be
// look at ways to helping out with traces or functionaility executions steps.
// ...

// create a auto dump interface that is easy to drop interfaces in to process.
// also a Debug1 Debug2 funcs
// consider a new handler interface for what I really need.
// or some helper funcs to do the job.

// the catch maybe be that the interface can't see what I am providing. - but can I extend the interface?
func Debug1(Msg string) {
	// set fields to be level=1
	GetLogger().WithField("level", "1").Debug(Msg)
}

// Used for describing the Top Level.
func DebugTest(Msg string) {
	// set fields to be level=999   - Top level
	GetLogger().WithField("level", "999").Debug(Msg)
}

func TestCleanup(Msg string, err error) {
	// set fields to be level=999   - Top level
	GetLogger().WithField("level", "990").Debug(fmt.Sprintf("Test Cleanup %s %v", Msg, err))
}

//func Debug1(Msg string, v ...string) {
// // set fields to be level=1
// logger.WithField("level", "1").Debug(Msg)
//}

func DebugBO(Msg string, a LogAdapter) {
	//Z_LogAdapter(i interface{}) (LogAdapter, bool)
	GetLogger().WithFields(log.Fields{"type": a.AsType(), "pk": a.AsPK(), "rc": a.AsRC()}).Debug(Msg)
}

func Debug2(Msg string) {
	// set fields to be level=1
	GetLogger().WithField("level", "2").Debug(Msg)
}

// DM Layer - reported debug layer
func DebugDM(Msg string) {
	// set fields to be level=1
	GetLogger().WithField("level", "2").Debug(Msg)
}

// WriteError only writes if an error if presented
func WriteError(n int, err error) {
	if err != nil {
		GetLogger().Error(fmt.Sprintf("WriteError %v Bytes written, %v", n, err))
	}
}

func WriteaError(err error) {
	if err != nil {
		GetLogger().Error(fmt.Sprintf("WriteError %v", err))
	}
}

func Error(Msg string) {
   GetLogger().Error(fmt.Sprintf("WriteError %v", Msg))
}

func Info(Msg string) {
	GetLogger().Info(fmt.Sprintf("Info %v", Msg))
}

func Pass(Msg string) {
	// set fields to be level=1
	GetLogger().WithField("status", "pass").Debug(Msg)
}
