package ZLogger

import (
	"testing"
	"fmt"
)

type TestLog struct {
	key string
	init string
}

type ITesting interface {
	Key() string
	Init() string
}

func (t *TestLog) Key() string {
	return t.key
}

func (t *TestLog) Init() string {
	return t.init
}

type zContact struct {
	Contact_id int64
	Reccode string
}

func (c *zContact) AsPK() int64 {
	return c.Contact_id
}

func (c *zContact) AsType() string {
	return "zContact"
}

func (c *zContact) AsRC() string {
	return c.Reccode
}

// TestZ_LogAdapter : Testing that LogAdapter's are working as intended
func TestZ_LogAdapter(t *testing.T) {
	fmt.Println("TestZ_LogAdapter")

   InitLogger()
	Logit := GetLogger()

	DebugTest("Start.")

	Logit.Info("Statrtup...")

	// Some different examples of the with fields going - it does require a info... command after the withfields to write out correctly.
	as := TestLog{ key : "aaa", init : "ccc"}
	Logit.WithField("Key", as).Info("Key is as")

	key := "zzz"
	Logit.WithField("key", key).Warn("Key zzz is ") // Prints in red. - I like this
	Logit.WithField("Key", "aaa").Info("Key aaa is ")

	Debug1("Debug Line")
	Debug1("Debug Another Line")
	Debug2("Debug Line")
	Debug2("Debug Another Line")

	// Build a struct with an interface

	liContact := zContact{Contact_id:1, Reccode:"STAR001-1"}
	DebugBO("Retrieve", &liContact)

	// todo Load Log file to search for content.
	// To do a top level then setup a viewer to capture the top layer.

	DebugTest("End.")
}
