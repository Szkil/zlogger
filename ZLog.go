package ZLogger

import (
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"io"
	"os"
	"time"
)


//var IZLog IZLogger
//var ZLog *ZLogger   // Logger
var ZLog Logger   // previous interface support
var logger Logger  // interface instance
var zlogger *ZLogger // var for holding the instance.
// var log = logrus.New()

// todo: This needs to moved to a lower level of the application so it can be shared
//type ZLogger struct {
//	// apexLogger   hmm can't inherit it is not public.
//	// I need to build a new logger or simple get rid of ZLogger - which is not a bad idea though.
//	DoPrint bool // Print to Screen.   - this can move to something like test info or debug
//	DoLog   bool // file dumped
//	logger  *log.Logger
//
//}

//type IZLogger interface {
//	logging.Logger
//	LogMeStyle(style int, value string)
//	LogMe(value string)
//}

// Aim is to just ask for a set of predefined options
//sometimes we just want to screen log - sometimes it will be automated testing so off to a file for reports.

type IZLogOptions interface {
  IsStdIO() bool
  IsFile() bool
  GetFilename() string
  GetPath() string
}


// todo setup a reporting level - so bottom is including a trace. / debug level   Trace / Debug / Info Warnings Errors / Pass or Fail reporting
type ZLogOptions struct {
	StdIO bool
	FileIO bool
	Path string
	Filename string
	// Fileincrement name
	// Write level
}

func (self *ZLogOptions) IsStdIO() bool {return self.StdIO}
func (self *ZLogOptions) IsFile() bool {return self.FileIO}
func (self *ZLogOptions) GetFilename() string {return self.Filename}
func (self *ZLogOptions) GetPath() string {return  self.Path}

// Options defaults
func Options_Normal() *ZLogOptions {
   return &ZLogOptions{StdIO:true}
}

func Options_FileOnly() *ZLogOptions {
	return &ZLogOptions{StdIO:false, FileIO:true}
}

// To the console and to the file
func Options_Both(path string) *ZLogOptions {
	return &ZLogOptions{StdIO:false, FileIO:true, Path:path}
}

func Options_PathnFilename(path string, Filename string) *ZLogOptions {
	return &ZLogOptions{StdIO:false, FileIO:true, Path:path, Filename:Filename}
}


type Logger interface {
	Emergency(s string)
	Alert(s string)
	Critical(s string)
	Error(s string)
	Warn(s string)
	Notice(s string)
	Info(s string)
	Debug(s string)
	// logging.Logger
	WithError(err error) Logger
	WithField(key string, value interface{}) Logger
	WithFields(fields map[string]interface{}) Logger

	SetJsonFilename(filename string)
	Filename() string
	LOG() *log.Logger
}

type ZLogger struct {
	TheLogrus *log.Logger
	filename string
}

func (self *ZLogger) LOG() *log.Logger {
	return self.TheLogrus
}
func (self *ZLogger) SetJsonFilename(filename string) {
	self.filename = filename

	file, err := os.OpenFile(self.filename , os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		// assume multi io for the time being
	   mw := io.MultiWriter(os.Stdout, file)
	   self.TheLogrus.Out = mw
	} else {
		self.TheLogrus.Info("Failed to log to file, using default stderr")
	}

}

func (self *ZLogger) Filename() string {
	return self.filename
}

func (self *ZLogger) Emergency(s string) {
	// todo find out what Emergency is for
   // self.TheLogrus.
}

func (self *ZLogger) Alert(s string) {
   // self.TheLogrus.
}

func (self *ZLogger) Critical(s string) {

}

func (self *ZLogger) Error(s string) {
	self.TheLogrus.Error(s)
}

func (self *ZLogger) Warn(s string) {
   self.TheLogrus.Warn(s)
}

func (self *ZLogger) Notice(s string) {
   self.TheLogrus.Info(s)
}

func (self *ZLogger) Info(s string) {
	self.TheLogrus.Info(s)
}

func (self *ZLogger) Debug(s string) {
   self.TheLogrus.Debug(s)
}

// todo verify if Logger interface return is sufficient otherwise return the *Entry and work it further.
func (self *ZLogger) WithError(err error) Logger {
	self.TheLogrus.WithError(err)
	return self
}

func (self *ZLogger) WithField(key string, value interface{}) Logger {
	self.TheLogrus.WithField(key, value)
	return self
}

func (self *ZLogger) WithFields(fields map[string]interface{}) Logger {
	self.TheLogrus.WithFields(fields)
	return self
}

func GetLogger() Logger {
   if zlogger == nil {
   	InitLogger()
   }
   if zlogger == nil {
   	panic(errors.New("Logger has failed to Initialized"))
   }
	return zlogger
}

func InitLogger() {
	zlogger = new(ZLogger) // var for holding the instance.
	zlogger.TheLogrus = log.New()

	// if I set this I get it runn off the JSONFormatter
//	zlogger.TheLogrus.Formatter = &log.JSONFormatter{TimestampFormat:"2006-01-02 15:04:05"}
   // I need nano
zlogger.TheLogrus.Formatter = &log.JSONFormatter{TimestampFormat:time.RFC3339Nano}
	zlogger.TheLogrus.SetOutput(os.Stdout)
	zlogger.TheLogrus.SetLevel(log.DebugLevel)  // base level

	// if I set it this way given we are loging via zlogger.TheLogrus instance the follow doesnt work
	// Log as JSON instead of the default ASCII formatter.
	//log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	// See SetFilename for multiple writer example
	//log.SetOutput(os.Stdout)

	// todo change this so in the options so I can easily change it.
	// Only log the warning severity or above.
	//log.SetLevel(log.WarnLevel)

	ZLog = zlogger
}

//func NewZLogger() *ZLog {
//}
/*
func (self *ZLogger) LogMeStyle(style int, value string) {
	// Set the color we want.
	switch {
	case style == 0:
		ct.ResetColor()
	case style == 1:
		ct.ChangeColor(ct.Red, true, ct.Black, false)
	case style == 2:
		ct.ChangeColor(ct.Green, true, ct.Black, false)
	case style == 3:
		ct.ChangeColor(ct.Magenta, true, ct.Black, false)
	case style == 4:
		ct.ChangeColor(ct.Blue, true, ct.Black, false)
	case style == 5:
		ct.ChangeColor(ct.White, true, ct.Black, false)
	case style == 6:
		ct.ChangeColor(ct.White, true, ct.Green, false)

	}
	self.LogMe(value)
	ct.ResetColor()
}
*/
/*
// TODO: stdio coloring
// TODO: file output
// - buffer it then write it out? // open the file then write it then shut it down.
func (self *ZLogger) LogMe(value string) {
	if self.DoPrint {
		fmt.Print(value)
	}
	if self.DoLog {
		// write to a log file.
		//	 func New(out io.Writer, prefix string, flag int) *Logger
		f, err := os.OpenFile("WPServer.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			fmt.Println(fmt.Sprintf("Warning Logger Failed : Error opening file: %v ", err))
		} else {
			ZLog.logger = log.New(f, "", log.LstdFlags)
			defer f.Close()

			if self.logger != nil {
				self.logger.Print(value)
			}
		}

	}

	//logger := log.New(&buf, "logger: ", log.Lshortfile)
	//logger.Print("Hello, log file!")

}
*/

// Called to init the Logger
func Init() {
	InitLogger()
}